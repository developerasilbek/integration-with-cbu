package com.example.integrationcbu;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.ApiResponse;
import com.example.integrationcbu.payload.dto.CurrencyDTO;
import com.example.integrationcbu.payload.mapper.MapStructMapper;
import com.example.integrationcbu.repository.CurrencyRepository;
import com.example.integrationcbu.service.CurrencyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class CurrencyApiTest {
    private CurrencyService currencyService;

    private MapStructMapper mapStructMapper;

    private CurrencyRepository repository;

    @BeforeEach
    void setupService() {
        mapStructMapper = Mockito.mock(MapStructMapper.class);
        currencyService = Mockito.mock(CurrencyService.class);
        repository = Mockito.mock(CurrencyRepository.class);
    }

    @Test
    public void getAllCurrenciesAPI() {
        List<Currency> currencyList = new ArrayList<>();
        CurrencyDTO currencyDTO = new CurrencyDTO();
        currencyDTO.setId("1");
        currencyDTO.setCode("840");
        currencyDTO.setCcy("USD");
        currencyDTO.setCcyNmRU("Доллар США");
        currencyDTO.setCcyNmUZ("AQSH dollari");
        currencyDTO.setCcyNmUZC("АҚШ доллари");
        currencyDTO.setCcyNmEN("US Dollar");
        currencyDTO.setNominal("1");
        currencyDTO.setRate("11284.90");
        currencyDTO.setDiff("9.8");
        currencyDTO.setDate("07.12.2022");

        Currency currency = mapStructMapper.toEntity(currencyDTO);

        currencyList.add(currency);

        ArgumentCaptor<Currency> currencyArgumentCaptor = ArgumentCaptor.forClass(Currency.class);

        Mockito.verify(repository).save(currencyArgumentCaptor.capture());

        Currency value = currencyArgumentCaptor.getValue();
        Assertions.assertEquals(value, currency);

//        ApiResponse<List<Currency>> response = new ApiResponse<>(
//                "saved",
//                true,
//                currencyList
//        );
//
//        Mockito.when(currencyService.saveAllCurrencies())
//                .thenReturn(response);
    }

}
