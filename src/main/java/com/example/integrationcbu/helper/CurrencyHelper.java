package com.example.integrationcbu.helper;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.dto.CurrencyDTO;
import com.example.integrationcbu.payload.mapper.MapStructMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class CurrencyHelper {
    //    private static final RestTemplate restTemplate = new RestTemplate();
    @Value("${currency.cbu.url}")
    private String cbuURL;

    @Qualifier("cbuRestTemplate")
    private final RestTemplate cbuRestTemplate;

    private final MapStructMapper mapStructMapper;

    public CurrencyHelper(RestTemplate cbuRestTemplate, MapStructMapper mapStructMapper) {
        this.cbuRestTemplate = cbuRestTemplate;
        this.mapStructMapper = mapStructMapper;
    }

    public List<Currency> getCurrenciesFromCbu() {
        ResponseEntity<CurrencyDTO[]> response = cbuRestTemplate.getForEntity(cbuURL, CurrencyDTO[].class);
        CurrencyDTO[] body = response.getBody();
        if (body == null || body.length == 0) {
            return null;
        }
        return Arrays.stream(body)
                .map(mapStructMapper::toEntity)
                .toList();
    }

    public List<Currency> getCurrenciesFromAnor() {
        return null;
    }
}
