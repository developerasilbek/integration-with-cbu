package com.example.integrationcbu.config;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    @Bean(name = "cbuRestTemplate")
    public RestTemplate cbuRestTemplate() {
        return new RestTemplate();
    }

    @Bean(name = "anorRestTemplate")
    public RestTemplate anorRestTemplate() {
        return new RestTemplate();
    }
}
