package com.example.integrationcbu.repository;

import com.example.integrationcbu.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, Integer> {
    Currency findByCode(Integer code);
}
