package com.example.integrationcbu.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Currency {
    @Id
    private Integer id;

    @Column(nullable = false)
    private Integer code;

    @Column(nullable = false)
    private String ccy;

    @Column(nullable = false)
    private String ccyNmRU;

    @Column(nullable = false)
    private String ccyNmUZ;

    @Column(nullable = false)
    private String ccyNmUZC;

    @Column(nullable = false)
    private String ccyNmEN;

    @Column(nullable = false)
    private Integer nominal;

    @Column(nullable = false)
    private Double rate;

    @Column(nullable = false)
    private Double diff;

    @Column(nullable = false)
    private Date date;
}
