package com.example.integrationcbu.controller;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.ApiResponse;
import com.example.integrationcbu.service.CurrencyService;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CurrencyController {
    private final CurrencyService service;

    public CurrencyController(CurrencyService service) {
        this.service = service;
    }

    @GetMapping("/saveAll")
    public HttpEntity<ApiResponse<List<Currency>>> saveCurrency() {
        ApiResponse<List<Currency>> response = service.saveAllCurrencies();
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping("/getByCode/{code}")
    public HttpEntity<ApiResponse<Currency>> getByCode(@PathVariable Integer code) {
        ApiResponse<Currency> response = service.getByCode(code);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getAllCurrencies")
    public HttpEntity<ApiResponse<List<Currency>>> getAllCurrencies() {
        ApiResponse<List<Currency>> response = service.getAllCurrencies();
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

}
