package com.example.integrationcbu.payload.mapper;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.dto.CurrencyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MapStructMapper {
    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    Currency toEntity(CurrencyDTO currencyDTO);

    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    CurrencyDTO toDTO(Currency currency);
}
