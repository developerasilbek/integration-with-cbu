package com.example.integrationcbu.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse<PARAM> {
    private String message;
    private boolean success;
    private PARAM param;

    public ApiResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
        this.param=null;
    }

}
