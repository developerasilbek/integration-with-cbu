package com.example.integrationcbu.service.impl;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.ApiResponse;
import com.example.integrationcbu.repository.CurrencyRepository;
import com.example.integrationcbu.service.CurrencyService;
import com.example.integrationcbu.service.enums.ServiceName;
import com.example.integrationcbu.helper.CurrencyHelper;
import jakarta.transaction.Transactional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = CurrencyService.SERVICE_NAME)
@ConditionalOnProperty(prefix = "currency.service", value = "name", havingValue = "ANOR")
public class AnorCurrencyService implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    private final CurrencyHelper cbuHelper;

    public AnorCurrencyService(CurrencyRepository currencyRepository, CurrencyHelper commonUtills) {
        this.currencyRepository = currencyRepository;
        this.cbuHelper = commonUtills;
    }

    @Override
    public ApiResponse<Currency> saveCurrency(Currency body) {
        Currency currency = currencyRepository.save(body);
        return new ApiResponse<>("saved", true, currency);
    }

    @Transactional
    @Override
    public ApiResponse<List<Currency>> saveAllCurrencies() {
        List<Currency> currenciesFromCbu = cbuHelper.getCurrenciesFromCbu();
        if (currenciesFromCbu == null || currenciesFromCbu.size() == 0) {
            return new ApiResponse<>("no content", false);
        }
        List<Currency> currencyList = currencyRepository.saveAll(currenciesFromCbu);
        return new ApiResponse<>("all saved", true, currencyList);
    }

    @Override
    public ApiResponse<Currency> getByCode(Integer code) {
        System.out.println("Get By Code for ANOR");
        Currency currency = currencyRepository.findByCode(code);
        return new ApiResponse<>("byCode", true, currency);
    }

    @Override
    public ApiResponse<List<Currency>> getAllCurrencies() {
        List<Currency> all = currencyRepository.findAll();
        return new ApiResponse<>("all", true, all);
    }

    @Override
    public ServiceName stateServiceName() {
        return ServiceName.ANOR;
    }
}
