package com.example.integrationcbu.service.impl;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.ApiResponse;
import com.example.integrationcbu.repository.CurrencyRepository;
import com.example.integrationcbu.service.CurrencyService;
import com.example.integrationcbu.service.enums.ServiceName;
import com.example.integrationcbu.helper.CurrencyHelper;
import jakarta.transaction.Transactional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = CurrencyService.SERVICE_NAME)
@ConditionalOnProperty(prefix = "currency.service", value = "name", havingValue = "CBU", matchIfMissing = true)
public class CbuCurrencyService implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    private final CurrencyHelper cbuHelper;

    public CbuCurrencyService(CurrencyRepository currencyRepository, CurrencyHelper cbuHelper) {
        this.currencyRepository = currencyRepository;
        this.cbuHelper = cbuHelper;
    }

    @Override
    public ApiResponse<Currency> saveCurrency(Currency body) {
        Currency currency = currencyRepository.save(body);
        return new ApiResponse<>("saved", true, currency);
    }

    @Transactional
    @Override
    public ApiResponse<List<Currency>> saveAllCurrencies() {
        List<Currency> currenciesFromCbu = cbuHelper.getCurrenciesFromCbu();
        if (currenciesFromCbu == null || currenciesFromCbu.size() == 0) {
            return new ApiResponse<>("no content", false);
        }
        List<Currency> currencyList = currencyRepository.saveAll(currenciesFromCbu);
        return new ApiResponse<>("all saved", true, currencyList);
    }

    @Override
    public ApiResponse<Currency> getByCode(Integer code) {
        System.out.println("Get By Code for CBU");
        Currency currency = currencyRepository.findByCode(code);
        return new ApiResponse<>("byCode", true, currency);
    }

    @Override
    public ApiResponse<List<Currency>> getAllCurrencies() {
        List<Currency> currenciesFromCbu = cbuHelper.getCurrenciesFromCbu();
        if (currenciesFromCbu == null || currenciesFromCbu.size() == 0) {
            return new ApiResponse<>("no content", false);
        }
        ApiResponse<List<Currency>> response;
        if (currenciesFromCbu.get(0).getDate().getTime() - System.currentTimeMillis() >= 86400000) {
            response = new ApiResponse<>("all", true, currencyRepository.findAll());
        } else {
            response = new ApiResponse<>("all", true, currenciesFromCbu);
            currencyRepository.saveAll(currenciesFromCbu);
        }
        return response;
    }

    @Override
    public ServiceName stateServiceName() {
        return ServiceName.CBU;
    }
}
