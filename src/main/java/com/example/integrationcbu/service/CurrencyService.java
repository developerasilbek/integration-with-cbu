package com.example.integrationcbu.service;

import com.example.integrationcbu.entity.Currency;
import com.example.integrationcbu.payload.ApiResponse;
import com.example.integrationcbu.service.enums.ServiceName;

import java.util.List;

public interface CurrencyService {
    String SERVICE_NAME = "currencyService";

    ApiResponse<Currency> saveCurrency(Currency body);

    ApiResponse<List<Currency>> saveAllCurrencies();

    ApiResponse<Currency> getByCode(Integer code);

    ApiResponse<List<Currency>> getAllCurrencies();

    ServiceName stateServiceName();
}
