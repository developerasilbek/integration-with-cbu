package com.example.integrationcbu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrationCbuApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegrationCbuApplication.class, args);
    }

}
